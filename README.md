<small>
  I <b>Amir</b> of the house <b>Mohammadi</b>.
  <br>
  <b>Mobile and web Full stack developer</b>.
</small>
<br>
<br>
<small>
  For more information, check <a href="https://blackiq.ir">BlackIQ.ir</a>.
  <br>
  Also there is <a href="https://fa.blackiq.ir">fa.BlackIQ.ir</a> for persian language.
</small>
<br>
<br>
<small>
  Contact via Email <a href="mailto:me@blackiq.ir">me@blackiq.ir</a>
</small>
